package id.ac.ui.cs.tutorial0.controller;

import id.ac.ui.cs.tutorial0.service.AdventurerCalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdventurerController {

    @Autowired
    //autowired cari otomatis kelas yg mengimplement interface yg terdeclare
    //jadi inisiasi ga manual
    private AdventurerCalculatorService adventurerCalculatorService;

    @RequestMapping("/adventurer/countPower")
    private String showAdventurerPowerFromBirthYear(@RequestParam("birthYear")int birthYear, Model model) {
    	//parameter Model model untuk pass data ke view ke file html
        //http://localhost:8080/adventurer/countPower?birthYear=2000 contohnya
    	//variable namanya power dengan nilanya hasil itung2an countPowerPotensialFromBirthYear
        model.addAttribute("power", adventurerCalculatorService.countPowerPotensialFromBirthYear(birthYear));
        model.addAttribute("tipeKelas", adventurerCalculatorService.powerClassifier(adventurerCalculatorService.countPowerPotensialFromBirthYear(birthYear)));
        return "calculator";
    }
}
