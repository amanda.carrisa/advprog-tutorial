package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;


@Service
//class yg implement interfacenya ada @service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        //Menerima input dan itung kekuatan adventurer lalu hasil di send ke html
        if (rawAge<30) {
            return rawAge*2000;
        } else if (rawAge <50) {
            return rawAge*2250;
        } else {
            return rawAge*5000;
        }
    }

    @Override
    public String powerClassifier(int power){
        String tipe = "";
        if(power >= 0 && power < 20000){
            tipe = "C class";
        }
        else if(power >= 20000 && power < 100000){
            tipe = "B class";
        }
        else if(power >= 100000){
            tipe = "A class";
        }
        return tipe;
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
