package id.ac.ui.cs.tutorial0;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//main class spring untuk jalanin aplikasi yang udah dibuat
public class Tutorial0Application {

    public static void main(String[] args) {
        SpringApplication.run(Tutorial0Application.class, args);
    }

}
