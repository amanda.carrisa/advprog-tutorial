package id.ac.ui.cs.tutorial0.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
//controller untuk kalo ada yang jika request ke aplikasi dan beri response pada requestnya
public class MainController {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    //ngatur request yang dihandle kek method apa post get dl dan url yg dihandle juga.
    //method homenya .GET value / jadi akses ke url path / dengan method get
    private String home() {
        return "home";
        //controller return string
    }

    @RequestMapping(method = RequestMethod.GET, value = "/greet")
    private String greetingsWithRequestParam(@RequestParam("name")String name, Model model) {
        //ini jadi requestparam itu ?namaParam=suatu nilai jadi /greet?name=sesuatu
    	//jadi yang dalam @reqmapping nama param dlm requestnya
    	//kalau gk mau bisa @reqmapping(required=false), defaultnya true tapi
        model.addAttribute("name", name);
        return "home";
    }
}
