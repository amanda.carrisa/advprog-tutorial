package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //ToDo: Complete Me
        this.guild = guild;
        this.guild.add(this);
    }

    //ToDo: Complete Me
    public void update(){
        Quest addedQuest = guild.getQuest();
         if(guild.getQuestType().equalsIgnoreCase("D")|| guild.getQuestType().equalsIgnoreCase("R")){
             getQuests().add(addedQuest); //getQuest() itu dari adventurer, return quest
         }
    };
}
